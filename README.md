# Degenerator

This is a simple mod to disable vanilla Minecraft world generation.

Some but not all of these items can be disabled via vanilla generator settings or other mods.
This mod aims to offer comprehensive control over vanilla world generation.

Generation is configured via rules in config/degenerator.cfg.
There are four sections for of rules (see categories below.)

## Examples

To disable silverfish everywhere (they only spawn in extreme hills by default):

```
S:minables <
  SILVERFISH
>
```

To disable melons, disable pumpkins in jungles:

```
S:decorators <
  {"type": "PUMPKIN", "biomes": ["JUNGLE", "Jungle", "JungleHills", "JungleEdge", "Jungle M", "JungleEdge M"]}
>
```

To disable gigantic mushrooms everywhere in the overworld except mushroom biomes, use two rules:

```
S:decorators <
  {"type": "BIG_SHROOM", "biomes": "MUHSROOM", "dimensions": [0], "result": "ALLOW"}
  {"type": "BIG_SHROOM", "dimensions": [0], "result": "DENY"}
>
```

## Rules

There are four types of rules: decorators, features, minables, and populators.
Each type corresponds to a category below and to a distinct Forge event type.

At its simplest, a rule is the name of the item to disable placed in the correct list.
For example, if you want to disable emeralds, put the line EMERALD in the minables list.

Rules can also be JSON objects with the following fields:

+ type (required, string): The name of the item to disable.
+ biomes (optional, array of strings, default: []): List of biome names/types this rule applies to. Empty means all biomes.
+ dimensions (optional, array of integers, default: []): List of dimensions this rule applies to. Empty means all dimensions.
+ log (optional, boolean, default: false): Log when a rule matches and the event result.
+ result (optional, string, default: "DENY"): How a matching rule should treat the generation event. Can be "ALLOW", "DENY", or "DEFAULT". 

When a rule is specified as just a type (e.g EMERALD) that is equivalent to the following JSON rule:

```json
{"type": "EMERALD", "biomes": [], "dimensions": [], "log": false, "result": "DENY"}
```

You don't put commas between rules.
Each rule goes on its own line.
The first rule that matches and has a non-"DEFAULT" result wins.

## Categories

There are four categories of generation that can be controlled.

These utilize Minecraft Forge EventType enums for the corresponding event types and are not hard-coded.
Thus if Forge expands to these events, this mod can will recognize the new types automatically. 

+ decorators (DecorateBiomeEvent.Decorate)
  - BIG_SHROOM
  - CACTUS
  - CLAY
  - DEAD_BUSH
  - DESERT_WELL
  - LILYPAD
  - FLOWERS
  - FOSSIL
  - GRASS
  - ICE
  - LAKE_WATER
  - LAKE_LAVA
  - PUMPKIN
  - REED
  - ROCK
  - SAND
  - SAND_PASS2
  - SHROOM
  - TREE
  - CUSTOM
+ features (InitMapGenEvent)
  - CAVE
  - MINESHAFT
  - NETHER_BRIDGE
  - NETHER_CAVE
  - RAVINE
  - SCATTERED_FEATURE
  - STRONGHOLD
  - VILLAGE
  - OCEAN_MONUMENT
  - WOODLAND_MANSION
  - END_CITY
  - CUSTOM
+ minables (OreGenEvent.GenerateMinable)
  - COAL
  - DIAMOND
  - DIRT
  - GOLD
  - GRAVEL
  - IRON
  - LAPIS
  - REDSTONE
  - QUARTZ
  - DIORITE
  - GRANITE
  - ANDESITE
  - EMERALD
  - SILVERFISH
  - CUSTOM
+ populators (PopulateChunkEvent.Populate)
  - DUNGEON
  - FIRE
  - GLOWSTONE
  - ICE
  - LAKE
  - LAVA
  - NETHER_LAVA
  - NETHER_LAVA2
  - NETHER_MAGMA
  - ANIMALS
  - CUSTOM
