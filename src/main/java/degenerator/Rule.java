package degenerator;

import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.fml.common.eventhandler.Event;

public class Rule<T extends Enum<T>>
{
    public final T type;
    // todo: add chance?
    // todo: match biome class name or super class?
    public final Set<String> biomeNames;
    public final Set<BiomeDictionary.Type> biomeTypes;
    public final Set<Integer> dimensions;
    public final boolean matchBiomeNames;
    public final boolean matchBiomeTypes;
    public final boolean matchDimensions;
    public final boolean log;
    public final Event.Result result;

    public Rule(T type, String[] biomes, Integer[] dimensions, boolean log, String result)
    {
        Set<String> abt = BiomeDictionary.Type.getAll().stream().map(BiomeDictionary.Type::getName).collect(Collectors.toSet());
        this.type = type;
        biomeNames = new HashSet<>();
        biomeTypes = new HashSet<>();
        for (String biome: biomes) {
            if (abt.contains(biome)) {
                // name is registered in BiomeDictionary: BIOME_TYPE
                biomeTypes.add(BiomeDictionary.Type.getType(biome));
            } else {
                // name not registered in BiomeDictionary: BiomeName
                biomeNames.add(biome);
            }
        }
        this.dimensions = new HashSet<>(Arrays.asList(dimensions));
        matchBiomeNames = !biomeNames.isEmpty();
        matchBiomeTypes = !biomeTypes.isEmpty();
        matchDimensions = !this.dimensions.isEmpty();
        this.log = log;
        this.result = Event.Result.valueOf(result);
    }

    public Event.Result apply(@Nullable World world, @Nullable BlockPos pos)
    {
        if (world == null || pos == null || !(matchBiomeNames || matchBiomeTypes || matchDimensions)) {
            if (log) {
                if (world == null || pos == null) {
                    Degenerator.LOG.info(String.format("%s.%s: %s", shorten(type.getClass().getName()), type.name(), result.name()));
                } else {
                    Degenerator.LOG.info(String.format("%s.%s: %s [d=%d x=%d y=%d z=%d]", shorten(type.getClass().getName()), type.name(), result.name(), world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ()));
                }
            }
            return result;
        }
        boolean matchedBiomeName = false;
        boolean matchedBiomeType = false;
        boolean matchedDimension = false;
        if (matchBiomeNames || matchBiomeTypes) {
            Biome biome = world.getBiome(pos);
            if (matchBiomeNames && biomeNames.contains(biome.getBiomeName())) {
                matchedBiomeName = true;
            }
            if (matchBiomeTypes) {
                for (BiomeDictionary.Type type : BiomeDictionary.getTypes(biome)) {
                    if (biomeTypes.contains(type)) {
                        matchedBiomeType = true;
                        break;
                    }
                }
            }
        }
        if (matchDimensions && dimensions.contains(world.provider.getDimension())) {
            matchedDimension = true;
        }
        if ((!matchBiomeNames || matchedBiomeName) && (!matchBiomeTypes || matchedBiomeType) && (!matchDimensions || matchedDimension)) {
            if (log) {
                Degenerator.LOG.info(String.format("%s.%s: %s [d=%d x=%d y=%d z=%d]", shorten(type.getClass().getName()), type.name(), result.name(), world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ()));
            }
            return result;
        }
        return Event.Result.DEFAULT;
    }

    private static String shorten(String name)
    {
        return name.substring(name.lastIndexOf('.') + 1).replace('$', '.');
    }

}
