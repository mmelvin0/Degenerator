package degenerator;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.util.*;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.event.terraingen.*;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = Degenerator.MODID)
@Mod.EventBusSubscriber(modid = Degenerator.MODID)
public class Options
{

    @Config.Comment("Rules for biome decorators (DecorateBiomeEvent.EventType)")
    @Config.LangKey("degenerator.config.decorators")
    public static String[] decorators = new String[] {};

    @Config.Ignore
    public static Map<DecorateBiomeEvent.Decorate.EventType, List<Rule<DecorateBiomeEvent.Decorate.EventType>>> decoratorRules;


    @Config.Comment("Rules for map features (InitMapGenEvent.EventType)")
    @Config.LangKey("degenerator.config.features")
    @Config.RequiresWorldRestart
    public static String[] features = new String[] {};

    @Config.Ignore
    public static Map<InitMapGenEvent.EventType, List<Rule<InitMapGenEvent.EventType>>> featureRules;


    @Config.Comment("Rules for minable generation (GenerateMinable.EventType)")
    @Config.LangKey("degenerator.config.minables")
    public static String[] minables = new String[] {};

    @Config.Ignore
    public static Map<OreGenEvent.GenerateMinable.EventType, List<Rule<OreGenEvent.GenerateMinable.EventType>>> minableRules;


    @Config.Comment("Rules for chunk populators (PopulateChunkEvent.Populate.EventType)")
    @Config.LangKey("degenerator.config.populators")
    public static String[] populators = new String[] {};

    @Config.Ignore
    public static Map<PopulateChunkEvent.Populate.EventType, List<Rule<PopulateChunkEvent.Populate.EventType>>> populatorRules;

    @SubscribeEvent
    public static void onChange(ConfigChangedEvent.OnConfigChangedEvent event)
    {
        if (Degenerator.MODID.equals(event.getModID())) {
            ConfigManager.sync(Degenerator.MODID, Config.Type.INSTANCE);
        }
    }

    @SubscribeEvent
    public static void onChanged(ConfigChangedEvent.PostConfigChangedEvent event)
    {
        compute();
    }

    public static void compute()
    {
        decoratorRules = compute(DecorateBiomeEvent.Decorate.EventType.class, decorators);
        featureRules = compute(InitMapGenEvent.EventType.class, features);
        minableRules = compute(OreGenEvent.GenerateMinable.EventType.class, minables);
        populatorRules = compute(PopulateChunkEvent.Populate.EventType.class, populators);
    }

    private static <T extends Enum<T>> Map<T, List<Rule<T>>> compute(Class<T> type, String[] inputs)
    {
        Map<T, List<Rule<T>>> rules = new HashMap<>();
        Arrays.stream(inputs).forEach(input -> parse(type, input.trim(), rules));
        return rules;
    }

    private static <T extends Enum<T>> void parse(Class<T> type, String input, Map<T, List<Rule<T>>> rules)
    {
        Entry entry;
        if (input.startsWith("{")) {
            try {
                entry = new Gson().fromJson(input, Entry.class);
            } catch (JsonSyntaxException jse) {
                Degenerator.LOG.error(String.format("invalid rule json: %s: %s", jse, input));
                return;
            }
        } else {
            entry = new Entry();
            entry.type = input;
        }
        Rule<T> rule;
        try {
            rule = new Rule<>(Enum.valueOf(type, entry.type), entry.biomes, entry.dimensions, entry.log, entry.result);
        } catch (RuntimeException ex) {
            Degenerator.LOG.error(String.format("invalid rule: %s: %s", ex, input));
            return;
        }
        if (!rules.containsKey(rule.type)) {
            rules.put(rule.type, new ArrayList<>());
        }
        rules.get(rule.type).add(rule);
    }

    private static class Entry
    {
        public String type;
        public String[] biomes = new String[] {};
        public Integer[] dimensions = new Integer[] {};
        public boolean log = false;
        public String result = "DENY";
    }

}
