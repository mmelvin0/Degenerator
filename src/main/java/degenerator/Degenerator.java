package degenerator;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = Degenerator.MODID, name = Degenerator.NAME, version = Degenerator.VERSION)
public class Degenerator
{
    public static final String MODID = "degenerator";
    public static final String NAME = "Degenerator";
    public static final String VERSION = "0.0.1";
    public static final Logger LOG = LogManager.getLogger(MODID);

    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        Options.compute();
        MinecraftForge.ORE_GEN_BUS.register(Listener.class);
        MinecraftForge.TERRAIN_GEN_BUS.register(Listener.class);
    }

}
