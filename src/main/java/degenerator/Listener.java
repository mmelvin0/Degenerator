package degenerator;

import java.util.List;
import java.util.Random;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.ChunkPrimer;
import net.minecraft.world.gen.MapGenBase;
import net.minecraft.world.gen.structure.*;
import net.minecraftforge.event.terraingen.*;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class Listener
{

    @SubscribeEvent
    public static void onDecorate(DecorateBiomeEvent.Decorate event)
    {
        List<Rule<DecorateBiomeEvent.Decorate.EventType>> rules = Options.decoratorRules.get(event.getType());
        if (rules == null || rules.isEmpty()) {
            return;
        }
        BlockPos pos = event.getPlacementPos();
        if (pos == null) {
            pos = event.getChunkPos().getBlock(0, 0, 0);
        }
        Event.Result result = apply(rules, event.getWorld(), pos);
        if (result != Event.Result.DEFAULT) {
            event.setResult(result);
        }
    }

    @SubscribeEvent
    public static void onFeature(InitMapGenEvent event)
    {
        List<Rule<InitMapGenEvent.EventType>> rules = Options.featureRules.get(event.getType());
        if (rules == null || rules.isEmpty()) {
            return;
        }
        if (apply(rules, null, null) == Event.Result.DENY) {
            event.setNewGen(getDummyMapGen(event));
        }
    }

    @SubscribeEvent
    public static void onMinable(OreGenEvent.GenerateMinable event)
    {
        List<Rule<OreGenEvent.GenerateMinable.EventType>> rules = Options.minableRules.get(event.getType());
        if (rules == null || rules.isEmpty()) {
            return;
        }
        Event.Result result = apply(rules, event.getWorld(), event.getPos());
        if (result != Event.Result.DEFAULT) {
            event.setResult(result);
        }
    }

    @SubscribeEvent
    public static void onPopulate(PopulateChunkEvent.Populate event)
    {
        List<Rule<PopulateChunkEvent.Populate.EventType>> rules = Options.populatorRules.get(event.getType());
        if (rules == null || rules.isEmpty()) {
            return;
        }
        Event.Result result = apply(rules, event.getWorld(), new ChunkPos(event.getChunkX(), event.getChunkZ()).getBlock(0, 0, 0));
        if (result != Event.Result.DEFAULT) {
            event.setResult(result);
        }
    }

    private static <T extends Enum<T>> Event.Result apply(List<Rule<T>> rules, World world, BlockPos pos)
    {
        for (Rule<T> rule : rules) {
            Event.Result result = rule.apply(world, pos);
            if (result != Event.Result.DEFAULT) {
                return result;
            }
        }
        return Event.Result.DEFAULT;
    }

    private static MapGenBase getDummyMapGen(InitMapGenEvent event)
    {
        switch (event.getType()) {
            case END_CITY:
                return new MapGenEndCity(ObfuscationReflectionHelper.getPrivateValue(MapGenEndCity.class, (MapGenEndCity)event.getOriginalGen(), "endProvider", "field_186133_d", "d")) {
                    @Override protected boolean canSpawnStructureAtCoords(int x, int z) { return false; }
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override public synchronized boolean generateStructure(World world, Random rand, ChunkPos pos) { return false; }
                    @Override public BlockPos getNearestStructurePos(World world, BlockPos pos, boolean findUnexplored) { return null; }
                    @Override protected StructureStart getStructureAt(BlockPos pos) { return null; }
                    @Override public boolean isInsideStructure(BlockPos pos) { return false; }
                    @Override public boolean isPositionInStructure(World world, BlockPos pos) { return false; }
                };
            case MINESHAFT:
                return new MapGenMineshaft() {
                    @Override protected boolean canSpawnStructureAtCoords(int x, int z) { return false; }
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override public synchronized boolean generateStructure(World world, Random rand, ChunkPos pos) { return false; }
                    @Override public BlockPos getNearestStructurePos(World world, BlockPos pos, boolean findUnexplored) { return null; }
                    @Override protected StructureStart getStructureAt(BlockPos pos) { return null; }
                    @Override public boolean isInsideStructure(BlockPos pos) { return false; }
                    @Override public boolean isPositionInStructure(World world, BlockPos pos) { return false; }
                };
            case NETHER_BRIDGE:
                return new MapGenNetherBridge() {
                    @Override protected boolean canSpawnStructureAtCoords(int x, int z) { return false; }
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override public synchronized boolean generateStructure(World world, Random rand, ChunkPos pos) { return false; }
                    @Override public BlockPos getNearestStructurePos(World world, BlockPos pos, boolean findUnexplored) { return null; }
                    @Override protected StructureStart getStructureAt(BlockPos pos) { return null; }
                    @Override public boolean isInsideStructure(BlockPos pos) { return false; }
                    @Override public boolean isPositionInStructure(World world, BlockPos pos) { return false; }
                };
            case OCEAN_MONUMENT:
                return new StructureOceanMonument() {
                    @Override protected boolean canSpawnStructureAtCoords(int x, int z) { return false; }
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override public synchronized boolean generateStructure(World world, Random rand, ChunkPos pos) { return false; }
                    @Override public BlockPos getNearestStructurePos(World world, BlockPos pos, boolean findUnexplored) { return null; }
                    @Override protected StructureStart getStructureAt(BlockPos pos) { return null; }
                    @Override public boolean isInsideStructure(BlockPos pos) { return false; }
                    @Override public boolean isPositionInStructure(World world, BlockPos pos) { return false; }
                };
            case SCATTERED_FEATURE:
                return new MapGenScatteredFeature() {
                    @Override protected boolean canSpawnStructureAtCoords(int x, int z) { return false; }
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override public synchronized boolean generateStructure(World world, Random rand, ChunkPos pos) { return false; }
                    @Override public BlockPos getNearestStructurePos(World world, BlockPos pos, boolean findUnexplored) { return null; }
                    @Override protected StructureStart getStructureAt(BlockPos pos) { return null; }
                    @Override public boolean isInsideStructure(BlockPos pos) { return false; }
                    @Override public boolean isPositionInStructure(World world, BlockPos pos) { return false; }
                    @Override public boolean isSwampHut(BlockPos pos) { return false; }
                };
            case STRONGHOLD:
                return new MapGenStronghold () {
                    @Override protected boolean canSpawnStructureAtCoords(int x, int z) { return false; }
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override public synchronized boolean generateStructure(World world, Random rand, ChunkPos pos) { return false; }
                    @Override public BlockPos getNearestStructurePos(World world, BlockPos pos, boolean findUnexplored) { return null; }
                    @Override protected StructureStart getStructureAt(BlockPos pos) { return null; }
                    @Override public boolean isInsideStructure(BlockPos pos) { return false; }
                    @Override public boolean isPositionInStructure(World world, BlockPos pos) { return false; }
                };
            case VILLAGE:
                return new MapGenVillage() {
                    @Override protected boolean canSpawnStructureAtCoords(int x, int z) { return false; }
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override public synchronized boolean generateStructure(World world, Random rand, ChunkPos pos) { return false; }
                    @Override public BlockPos getNearestStructurePos(World world, BlockPos pos, boolean findUnexplored) { return null; }
                    @Override protected StructureStart getStructureAt(BlockPos pos) { return null; }
                    @Override public boolean isInsideStructure(BlockPos pos) { return false; }
                    @Override public boolean isPositionInStructure(World world, BlockPos pos) { return false; }
                };
            case WOODLAND_MANSION:
                return new WoodlandMansion(ObfuscationReflectionHelper.getPrivateValue(WoodlandMansion.class, (WoodlandMansion)event.getOriginalGen(), "provider", "field_191075_h", "h")) {
                    @Override protected boolean canSpawnStructureAtCoords(int x, int z) { return false; }
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override public synchronized boolean generateStructure(World world, Random rand, ChunkPos pos) { return false; }
                    @Override public BlockPos getNearestStructurePos(World world, BlockPos pos, boolean findUnexplored) { return null; }
                    @Override protected StructureStart getStructureAt(BlockPos pos) { return null; }
                    @Override public boolean isInsideStructure(BlockPos pos) { return false; }
                    @Override public boolean isPositionInStructure(World world, BlockPos pos) { return false; }
                };
            default:
                return new MapGenBase() {
                    @Override public void generate(World world, int x, int z, ChunkPrimer primer) {}
                    @Override protected void recursiveGenerate(World world, int chunkX, int chunkZ, int originalX, int originalZ, ChunkPrimer primer) {}
                };
        }
    }

}
